# CPS SDK

## Android

### Overview

Combain Positioning System (CPS) SDK for Android allows for accurate real time indoor positioning in 3D - globally!

Our positioning research since 2006 has resulted in advanced algorithms that gives CPS users world’s best accuracy.
Benchmarking results of Wi-Fi positioning median error are about 2-4x better than any other supplier.
This SDK is based on our research with Lund University and gives for most buildings below 10 meters accuracy for Wi-Fi positioning – about 5x better
than any global Wi-Fi positioning service today!

The SDK utilizes existing Wi-Fi access points and Bluetooth beacons together with sensors on the phone to provide as good position as possible.
The system is self learning so it gets more accurate every time it is used in a specific place.
The SDK will, after being used a few times in a building, also give the estimated floor inside the building (Ground floor is assumed to be floor 0).

The SDK needs a network connection to work, since some of the positioning is made on Combain location servers.

All the location data sent to the location servers is anonymous and just contains the app id and a random id that is updated every time the SDK starts and
every time the SDK wakes up from deep sleep. Thus there is no personal data sent or collected. The anonymous location data is stored and used for
improving the accuracy of the location service.

To conserve power, the SDK only use phone resources when needed. The idea is to be able to use the SDK for continuous tracking.
The SDK works in any environment, outdoor or indoor, for any moving patterns, moving or still, for any motion mode, vehicle or walking or biking.
To get best performance, the SDK should be run continuously since location history data improves the accuracy.
Starting the SDK deep inside a new building can give an erroneous or not found location.

### Features

- Subscribe/unsubscribe to position updates
- Get information about powersave modes
- Automatic deep sleep mode for low power
- Background mode

### Requirements

- Android SDK version 7+
- Mobile device with Wi-Fi enabled, ideally also BT/LTE/GPS
- Permissions to access sensors; accelerometer, gyrator, pressure sensor

#### Setup

Add the following in the `build.gradle` file for the Project under `allprojects -> repositories`:

```gradle
allprojects {
    repositories {
        ...
        maven {
            url 'https://gitlab.com/api/v4/projects/3194773/packages/maven'
        }
    }
}
```

Add the following in the `build.gradle` file for the Module under dependencies:

```gradle
var slamSDKVersion = "3.1.18"
dependencies {
    ...
    implementation 'com.combain:slamsdk-next:' + slamSDKVersion
    implementation 'com.combain:sdk-sensor-collection:' + slamSDKVersion
}
```

See section `Revision History` at the bottom for versions

Add the following permissions in AndroidManifest.xml:

```yml
<uses-permission
android:name="android.permission.BLUETOOTH"
android:maxSdkVersion="34"/>
<uses-permission
android:name="android.permission.BLUETOOTH_ADMIN"
android:maxSdkVersion="34"/>
<uses-permission android:name="android.permission.INTERNET"/>
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE"/>
<uses-permission android:name="android.permission.BLUETOOTH_CONNECT"/>
<uses-permission android:name="android.permission.BLUETOOTH_SCAN"/>
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION"/>
<uses-permission android:name="android.permission.ACCESS_FINE_LOCATION"/>
<uses-permission android:name="android.permission.ACTIVITY_RECOGNITION"/>
<uses-permission android:name="android.permission.CHANGE_NETWORK_STATE"/>
<uses-permission android:name="android.permission.WRITE_SETTINGS"/>
```

    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />

```
To initiate the SDK use the constructor:
```

public CPSSDK(Context c, String apiKey)

````
the apiKey is provided by Combain.

If required permissions are missing this function will throw a SecurityException.

### Usage
To initiate the SDK, a config first needs to be created. This is done with the `SlamSDKConfig.create` method.
**Example:**
```kotlin
val config = SlamSDKConfig.create(
    authenticationMode = AuthenticationMode.ApiKey("<YOUR_TOKEN>")
    )
````

For the complete documentation see: https://portal.combain.com/sdk/android/3.1.20/slamsdk-next/com.combain.slamsdk_next/-slam-s-d-k-config/

After this an instance of `SlamSDKCore` can be created:
**Example:**

```kotlin
val slamSDK = SlamSDKCore.create(context = this,config = config)
CoroutineScope(Dispatchers.Default).launch {
    slamSDK.start()
}
```

For the complete documentation see: https://portal.combain.com/sdk/android/3.1.20/slamsdk-next/com.combain.slamsdk_next/-slam-s-d-k-core/

When you have an instance of the `SlamSDK` you can subscribe to location updates by subscribing to the [StateFlow](https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/-state-flow/) `slamSDK.slamCpsService.location`
**Example:**

```kotlin
slamSDK.slamCpsService.location.collect{location -> ...}
```

You can also subscribe with callback with, `requestLocationUpdates`:
**Example:**

```kotlin
slamSDK.slamCpsService.requestLocationUpdates{location -> ...}
```

### Configuring modes

By default the SDK will switch between indoor, outdoor, passive and driving mode dynamicly depending on an estimation of the current state of the user. Each mode has different configured options for scan interval to provide a good balance between accuracy and battery saving.

If you wish to have a custom mode or a custom algorithm this can be configured by implementing the interface [SLModeManager](https://portal.combain.com/sdk/android/3.1.20/sdk-sensor-collection/com.combain.sdk_sensor_collection.mode/-s-l-mode-manager/?query=interface%20SLModeManager). Then creating and instance of this and passing it to the SlamSDKConfig.create method as the `modeManager` parameter.

If you wish to create custom modes with custom configured parameters for i.e scanning interval you can extend the class [SLMode](https://portal.combain.com/sdk/android/3.1.20/sdk-sensor-collection/com.combain.sdk_sensor_collection.mode.modes/-s-l-mode/)

### Full documentation

The full API reference for the SDK is available at https://portal.combain.com/sdk/android
