## 1. Generate `UUID` to use

For all your beacons in your organization you want to use the same `UUID.` If you do not have a `UUID` you want to use for all beacons generate one here [Online UUID Generator Tool](https://www.uuidgenerator.net/)

## Create table for info

My recommendation is to create a table where you fill in the table like this

- Location: A human readable note that helps you remember beacons location
- `UUID:` The `UUID` you generated in [[#1. Generate UUID to use]]
- **Major**: You want each physical location to have different Major
- **Minor**: Each beacon should have different minors
- **MAC**: The mac address of this beacon

| Location | UUID | Major | Minor | MAC |
| -------- | ---- | ----- | ----- | --- |
|          |      |       |       |     |

# Configure UUID, Major, Minor for MiniBeaconPlus

This is how you configure `UUID,` **Major**, **Minor** for [I3 Rugged Beacon](https://www.minew.com/product/i3-robust-beacon/), see the documentation for your respective beacon.

The first step is to find your beacons and set the `UUID,` **Major, Minor for them. To do this download this app:
iOS:**[BeaconSET Plus on the App Store](https://apps.apple.com/us/app/beaconset-plus/id1264548636)
**Android:** [BeaconSET Plus - Apps on Google Play](https://play.google.com/store/apps/details?id=com.minew.beaconplus&hl=en_CA)

For each of your beacons follow these steps.

### 1.

Read the `MAC` from your device, it is printed on the device and add an entry in your table for this device. Also select the **Major** and **Minor** in the table.

### 2.

Open the app and search for you device, see picture
![first](attachments/first.png)

### 3.

Click on your device when the dot is green and enter password `minew123`
![image](attachments/Pasted image 20241017194242.png)

### 4.

Go the tab `iBeacon` and enter your `UUID`, `Major` and `Minor`

## Place beacons

Now you beacons are ready to be places. Place them according to the location you noted in the table.

## Add beacons to Traxmate

Now you are ready to input the data into Traxmate. Go to your place in Traxmate and open `Positioning`, duplicate the model you want to use or create a new model and enter edit mode.

![image](attachments/Pasted image 20241017194641.png)

If your beacons are already added to the model find them and click on them, like shown in image above. If you have not added the beacons then you can add them by clicking Bluetooth symbol in UI.

Click the info symbol, you should be presented with this popup
![image](attachments/Pasted image 20241017194925.png)

Click `Beacon`

Change the option `Beacon` to `Yes`
![image](attachments/Pasted image 20241017194957.png)

Click `Show all attributes`
![image](attachments/Pasted image 20241017195057.png)

Now enter the `UUID,` **Major**, **Minor** corresponding to the device in the table.
![image](attachments/Pasted image 20241017195008.png)

Repeat this for all Beacons.

Now you should be ready to use `iBeacons` 🥳
