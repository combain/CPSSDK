# 3.1.20 - 2024-03-17

- Improved wifi scanning
- Support for custom pipeline modes
- SLMode can be overriden with custom configs


# Old

### Revision History. 

| Version | Date | Description |
| ---- | ---- | ---- |
| 1.0 | 2017-12-05 | Initial release |
| 1.3.4 | 2018-03-28 | Improved positioning, less data usage. |
| 1.5.0 | 2018-09-07 | Improved positioning, bug fixes. |
| 2.2.0 | 2020-10-01 | Improved power save. Added RTT. |
| 2.3.1 | 2021-04-13 | Improved IMU (heading, steps and walked distance). |
| 2.3.26 | 2022-10-11 | New step counter, SDK events, data classes from scans/CPS, scan requests, bug fixes & performance fixes. |
| 3.1.16 | 2024-02-01 | Overhauled SDK with improved performance and new API. |
| 3.1.17 | 2024-02-03 | Bugfixes |
| 3.1.18 | 2024-02-08 | Changed package structure to sepearte SlamSDK into two packages |